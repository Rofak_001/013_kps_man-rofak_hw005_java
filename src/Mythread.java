public class Mythread implements Runnable {
    private String str = "Hello KSHRD!";
    private String str1 = "****************************************";
    private String str2 = "I will try my best to be here HRD.";
    private String str3 = "----------------------------------------";
    private String str4 = "Downloading";
    private String str5 ="........";
    private String str6 = "Completed 100%!";
    @Override
    public void run() {
        showAnimateStr(str);
        System.out.println();
        showAnimateStr(str1);
        System.out.println();
        showAnimateStr(str2);
        System.out.println();
        showAnimateStr(str3);
        System.out.println();
        System.out.print(str4);
        showAnimateStr(str5);
        System.out.println(str6);
    }

    public void showAnimateStr(String string){
        for(int i=0;i<string.length();i++){
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print(string.charAt(i));
        }
    }
}
